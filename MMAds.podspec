
Pod::Spec.new do |s|

# 1
s.platform = :ios
s.ios.deployment_target = '10.0'
s.name = "MMAds"
s.summary = "MMAds provides advertisment from MonkingMe Platform as a distributor to your platforms."
s.requires_arc = true

# 2
s.version = "0.1.0"

# 3
s.license = { :type => "MIT", :file => "LICENSE" }

# 4 - Replace with your name and e-mail address
s.author = { "Guillem Budia" => "ios@monkingme.com" }

# 5 - Replace this URL with your own GitHub page's URL (from the address bar)
s.homepage = "https://www.monkingme.com"

# 6 - Replace this URL with your own Git URL from "Quick Setup"
s.source = { :git => "https://guilem_bt@bitbucket.org/monkingmedevelopers/mmads.git",
:tag => "#{s.version}" }

# 7
s.framework = "UIKit"
s.dependency 'RxCocoa', '~> 4.1'
s.dependency 'RxSwift', '~> 4.1'

# 8
s.source_files = "MMAds/**/*.{swift}"

# 9
s.resources = "MMAds/**/*.{png,jpeg,jpg,storyboard,xib,xcassets}"

# 10
s.swift_version = "4.2"

end
