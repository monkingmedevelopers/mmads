//
//  Utils.swift
//  ads
//
//  Created by Guillem Budia Tirado on 03/06/2019.
//  Copyright © 2019 MonkingMe. All rights reserved.
//

import Foundation

class Utils: NSObject {
    
    static func printDebug(sender: AnyObject?, message: Any){
        if let _sender = sender {
            print("["+String(describing: type(of: _sender))+"]: \(message)")
        } else {
            print("[]: \(message)")
        }
    }
    
    static func logDebug(sender: AnyObject?, message: Any){
        if let _sender = sender {
            let str = "["+String(describing: type(of: _sender))+"]: \(message)"
            NSLog(str)
        } else {
            NSLog("\(message)")
        }
    }
    
    static func printError(sender: AnyObject?, message: Any){
        if let _sender = sender {
            print("ERROR! -> ["+String(describing: type(of: _sender))+"]: \(message)")
        } else {
            print("ERROR! \(message)")
        }
    }
    
    static func logError(sender: AnyObject?, message: Any){
        
        if let _sender = sender {
            let str = "ERROR! -> ["+String(describing: type(of: _sender))+"]: \(message)"
            NSLog(str)
        } else {
            NSLog("ERROR! \(message)")
        }
    }
    
}
