//
//  MMAdsError.swift
//  ads
//
//  Created by Guillem Budia Tirado on 03/06/2019.
//  Copyright © 2019 MonkingMe. All rights reserved.
//

import Foundation

private struct ErrorTuple {
    var code: Int
    var description: String
}

public enum MMAdsError {
    case loadBeforeInitialisation
    
    public var code: Int {
        return getErrorTuple().code
    }
    
    public var description: String {
        return getErrorTuple().description
    }
    
    private func getErrorTuple() -> ErrorTuple {
        
        switch self {
        case .loadBeforeInitialisation:
            return ErrorTuple(code: 1,
                              description: "Ad has been tried to load without initialising the MonkingMe ads config.")
        }
    }
    
    
    
}
