//
//  AdQuizViewModel.swift
//  MMAds
//
//  Created by Guillem Budia Tirado on 07/06/2019.
//  Copyright © 2019 MonkingMe. All rights reserved.
//

import RxSwift

class AdQuizViewModel: NSObject {
    
    let taps: Observable<Int>
    private let _taps: Variable<Int>
    
    override init() {
        _taps = Variable(0)
        taps = _taps.asObservable()
        super.init()
    }
    
    func handleTap() {
        _taps.value += 1
    }

}
