//
//  AdQuizViewController.swift
//  MMAds
//
//  Created by Guillem Budia Tirado on 07/06/2019.
//  Copyright © 2019 MonkingMe. All rights reserved.
//

import UIKit
import RxSwift

class AdQuizViewController: UIViewController {
    
    @IBOutlet weak var tapsLabel: UILabel!
    
    private let viewModel = AdQuizViewModel()
    private let disposeBag = DisposeBag()
    
    init() {
        super.init(nibName: "AdQuizViewController",
                   bundle: Bundle(for: AdQuizViewController.self))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        bindTaps()
    }
    
    private func bindTaps() {
        viewModel.taps.subscribe(onNext: { [weak self] (taps) in
            self?.tapsLabel.text = "\(taps)"
        }).disposed(by: disposeBag)
    }

    @IBAction func tapsButtonAction(_ sender: Any) {
        viewModel.handleTap()
    }
    
    

}
