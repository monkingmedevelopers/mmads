//
//  MonkingMeAds.swift
//  ads
//
//  Created by Guillem Budia Tirado on 03/06/2019.
//  Copyright © 2019 MonkingMe. All rights reserved.
//

import Foundation
import UIKit

/// Static class that defines and sets the configurations for MonkingMe ads
public class MonkingMeAds {
    
    static var config: MMAdsConfig = MMAdsConfig()
    
    /// initialises the class config with the api key and the default config parameters
    ///
    /// - Parameter apiKey: client key to use MonkingMe ads
    public static func initWith(apiKey: String) {
        self.config = MMAdsConfig(apiKey: apiKey)
    }
    
    
    /// initialises the class with a congif object
    ///
    /// - Parameter config: configuration object
    public static func initWith(config: MMAdsConfig) {
        self.config = config
    }
    
    
    /// Gets the api key if defined
    ///
    /// - Returns: api key
    static func getApiKey() -> String? {
        
        let apiKey = config.apiKey
        return apiKey.isEmpty ? apiKey : nil
    }
    
    public static func showAdQuiz(in vc: UIViewController) {
        let vcReactive = AdQuizViewController()
        vc.present(vcReactive, animated: true, completion: nil)
    }
    
}
