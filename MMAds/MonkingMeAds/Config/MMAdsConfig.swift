//
//  MMAdsConfig.swift
//  ads
//
//  Created by Guillem Budia Tirado on 03/06/2019.
//  Copyright © 2019 MonkingMe. All rights reserved.
//

import Foundation

public struct MMAdsConfig {
    var apiKey: String = ""
}
