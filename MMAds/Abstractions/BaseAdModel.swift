//
//  BaseAdModel.swift
//  ads
//
//  Created by Guillem Budia Tirado on 03/06/2019.
//  Copyright © 2019 MonkingMe. All rights reserved.
//

import Foundation

/// Represents the basic info that the different ad models will have.
class BaseAdModel: NSObject {
    
    let sessionURL: URL
    
    init?(sessionURL: String) {
        
        guard let url = URL(string: sessionURL) else {
            return nil
        }
        
        self.sessionURL = url
        
        super.init()
    }
}
