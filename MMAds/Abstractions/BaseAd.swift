//
//  BaseAd.swift
//  ads
//
//  Created by Guillem Budia Tirado on 03/06/2019.
//  Copyright © 2019 MonkingMe. All rights reserved.
//

import Foundation

protocol BaseAd {
    
    associatedtype DelegateType
    
    static func setDelegate(_ delegate: DelegateType)
    
    static func isReady() -> Bool
    
    static func load()
    
    static func show(in viewController: UIViewController)
    
}
