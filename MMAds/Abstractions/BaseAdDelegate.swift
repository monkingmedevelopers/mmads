//
//  BaseAdDelegate.swift
//  ads
//
//  Created by Guillem Budia Tirado on 03/06/2019.
//  Copyright © 2019 MonkingMe. All rights reserved.
//

import Foundation

public protocol BaseAdDelegate: class {
    
    /// Notifies the delegate that an ad has been loaded successfully.
    func onLoadSuccess()
    
    /// Notifies the delegate that loading the ad has failed.
    ///
    /// - Parameter error: error type containing description and code.
    func onLoadError(_ error: MMAdsError)
    
    /// Notifies the delegate that the ad has been displayed.
    func onShow()
    
    /// Notifies the delegate that the ad has started.
    func onStarted()
    
    /// Notifies the delegate that the ad has finished.
    func onFinished()
    
    /// Notifies the delegate that a reward has been received.
    ///
    /// - Parameter reward: reward amount.
    func onRewardReceived(reward: Int?)
    
    /// Notifies the delegate that the call to action has been triggered.
    func onCallToAction()
    
}
