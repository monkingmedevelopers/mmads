//
//  BaseAdUnit.swift
//  ads
//
//  Created by Guillem Budia Tirado on 03/06/2019.
//  Copyright © 2019 MonkingMe. All rights reserved.
//

import Foundation

protocol BaseAdUnit {
    
    associatedtype DelegateType
    
    func setDelegate(_ delegate: DelegateType)
    
    func isReady() -> Bool
    
    func load()
    
    func show(in viewController: UIViewController)
}
